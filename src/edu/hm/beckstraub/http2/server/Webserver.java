/*
HTTP 2 Server and Client
Hochschule M�nchen, Fakut�t 07 f�r Mathematik und Informatik
Netzwerke II, SS2015
Author: Kevin Beck, Alexander Straub
Date: 15.06.2015
*/
package edu.hm.beckstraub.http2.server;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.ArrayUtils;

import edu.hm.beckstraub.http2.common.Connection;
import edu.hm.beckstraub.http2.common.Frame;
import edu.hm.beckstraub.http2.common.FrameType;

public class Webserver 
{
	public static void sendRepsonse(final Connection c, final Frame headerFrame) 
	{
        Thread t = new Thread(new Runnable()
        {
            public void run()
            {
					doResponse(c, headerFrame);
            }
        });
        t.start();
	}
	
	private static void doResponse(Connection c, Frame h)
	{
		// Read header
		String headerPayload = new String(h.getPayload());
		Path path = Paths.get("html/" + readHeader(headerPayload));
		
		// Try to load file
		File file = path.toFile();
		
		if (file.exists())
		{
			// Load file data
			byte[] data = new byte[0];
			try 
			{
				data = Files.readAllBytes(path);
			} 
			catch (IOException e)
			{
				System.out.println("Server Exception while loading file: " + e.getMessage());
				
				// Send 500 header
				String payload = "Status: 500 Internal Server Error";
				Frame frame = new Frame(FrameType.Headers, (byte)5, h.getStreamId(), payload.getBytes());
				c.sendFrame(frame);
			}
			
			// Send ok header
			String payload = "Status: 200 OK";
			Frame frame = new Frame(FrameType.Headers, (byte)4, h.getStreamId(), payload.getBytes());
			c.sendFrame(frame);
			
			// Send data
			frame = new Frame(FrameType.Data, (byte)1, h.getStreamId(), data);
			c.sendFrame(frame);
			
			// If it's a html file, send push promise files
			trySendPushPromise(c, file);
		}
		else
		{
			// Send 404 header
			String payload = "Status: 404 Not Found";
			Frame frame = new Frame(FrameType.Headers, (byte)5, h.getStreamId(), payload.getBytes());
			c.sendFrame(frame);
		}
	}
	
	private static String readHeader(String headerPayload)
	{
		String[] payloads = headerPayload.split(" ");
		String method = payloads[0].toUpperCase();
		
		if (method.equals("GET"))
		{
			if (payloads.length == 2)
			{
				return payloads[1];
			}
		}

		return "";
	}
	
	private static void trySendPushPromise(Connection c, File file)
	{
		try
		{
			if (file.getName().toLowerCase().endsWith("html"))
			{
				String content = readHtmlFile(file);

				String[] hrefFiles = getInlineFiles(content, "href");
				String[] srcFiles = getInlineFiles(content, "src");
				String[] inlineFiles = ArrayUtils.addAll(hrefFiles, srcFiles);
				
				for (String fileName: inlineFiles)
				{
					Path inlinePath = Paths.get(file.getParent() + "/" + fileName);
					File inlineFile = inlinePath.toFile();
					
					if (inlineFile.exists())
					{
						// Load file data
						byte[] data = new byte[0];
						try
						{
							data = Files.readAllBytes(inlinePath);
						}
						catch (IOException e)
						{
							System.out.println("Server Exception while loading Push Promise files: " + e.getMessage());
							continue;
						}

						int streamId = c.getNextStreamId();
						
						// Send Push Promise frame
						String payload = fileName;
						Frame frame = new Frame(FrameType.Push_Promise, (byte)4, streamId, payload.getBytes());
						c.sendFrame(frame);
						
						// Send data
						frame = new Frame(FrameType.Data, (byte)1, streamId, data);
						c.sendFrame(frame);
					}
					
					// Send recursive if it's a html file
					trySendPushPromise(c, inlineFile);
				}
			}
		}
		catch (IOException e)
		{
			System.out.println("Server Exception while sending Push Promise files: " + e.getMessage());
		}
	}
	
	private static String readHtmlFile(File htmlFile) throws IOException
	{
		StringBuilder contentBuilder = new StringBuilder();

	    BufferedReader in = new BufferedReader(new FileReader(htmlFile));
	    String str;
	    while ((str = in.readLine()) != null)
	    {
	        contentBuilder.append(str);
	    }
	    in.close();
		
		return contentBuilder.toString();
	}
	
	private static String[] getInlineFiles(String html, String attr)
	{
		List<String> inlineFiles = new ArrayList<>();
		
		String tagAttr = attr + "=\"";
		int nextIndex = html.indexOf(tagAttr, 0);
		while (nextIndex > 0)
		{
			int endIndex = html.indexOf("\"", nextIndex + tagAttr.length());
			
			String inlineFile = html.substring(nextIndex + tagAttr.length(), endIndex);
			inlineFiles.add(inlineFile);
			
			nextIndex = html.indexOf(tagAttr, endIndex);
		}
		
		return inlineFiles.toArray(new String[0]);
	}
}

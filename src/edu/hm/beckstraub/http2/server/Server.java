/*
HTTP 2 Server and Client
Hochschule M�nchen, Fakut�t 07 f�r Mathematik und Informatik
Netzwerke II, SS2015
Author: Kevin Beck, Alexander Straub
Date: 15.06.2015
*/
package edu.hm.beckstraub.http2.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import edu.hm.beckstraub.http2.common.Connection;
import edu.hm.beckstraub.http2.common.ConnectionUser;
import edu.hm.beckstraub.http2.common.Frame;
import edu.hm.beckstraub.http2.common.Stream;

public class Server implements ConnectionUser
{
	public static final int PORT = 8001;
	
	public Server() throws IOException
	{
		try(ServerSocket serverSocket = new ServerSocket(PORT))
		{
			System.out.println("Server is listening on port " + PORT + "...");
			
			while (true)
			{
				Socket clientSocket = serverSocket.accept();
				
				System.out.println("Connection established to client " + clientSocket.getInetAddress());
				
				Thread clientThred = new Thread(new Connection(clientSocket, this));
				clientThred.start();
			}
		}
	}
	
	@Override
	public void headerComplete(Connection c, Stream stream, Frame header) 
	{
		// Create a clever response by the header frame
		Webserver.sendRepsonse(c, header);
	}

	@Override
	public void dataComplete(Stream stream)
	{
		// Do nothing because server cannot receive data
	}
	
	public static void main(String[] args)
	{
		try
		{
			new Server();
		}
		catch (Exception e)
		{
			System.out.println("Server Exception: " + e.getMessage());
		}
	}
}

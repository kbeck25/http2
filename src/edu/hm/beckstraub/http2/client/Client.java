/*
HTTP 2 Server and Client
Hochschule M�nchen, Fakut�t 07 f�r Mathematik und Informatik
Netzwerke II, SS2015
Author: Kevin Beck, Alexander Straub
Date: 15.06.2015
*/
package edu.hm.beckstraub.http2.client;

import java.io.IOException;
import java.net.Socket;

import edu.hm.beckstraub.http2.server.Server;
import edu.hm.beckstraub.http2.common.Connection;
import edu.hm.beckstraub.http2.common.ConnectionUser;
import edu.hm.beckstraub.http2.common.Frame;
import edu.hm.beckstraub.http2.common.FrameType;
import edu.hm.beckstraub.http2.common.Stream;

public class Client implements ConnectionUser
{
	public Client() throws IOException
	{
		Socket socket = new Socket("localhost", Server.PORT);

		// Start thread to receive data
		Connection connection = new Connection(socket, this);
		Thread clientThread = new Thread(connection);
		clientThread.start();
		
		// Send request
		String payload = "GET /index.html";
		Frame frame = new Frame(FrameType.Headers, (byte)0, 0, payload.getBytes());
		connection.sendFrame(frame);		
		
		
		// Send ping
//		byte[] opaqueData = new byte[8];
//		Frame pingResponse = new Frame(FrameType.Ping, (byte)0, 0, opaqueData);
//		connection.sendFrame(pingResponse);
	}
	
	@Override
	public void headerComplete(Connection c, Stream stream, Frame header) 
	{
		// Do nothing
	}

	@Override
	public void dataComplete(Stream stream)
	{
		System.out.println("######################################## STREAM " + stream.getId() + " ########################################");
		System.out.println(new String(stream.getHeaderType() + ": " + stream.getHeader()));
		System.out.println("##########################################################################################");
		System.out.println(new String(stream.getData()));
		System.out.println();
	}
	
	public static void main(String[] args)
	{
		try
		{
			new Client();
		}
		catch (IOException e)
		{
			System.out.println("Client Exception: " + e.getMessage());
		}
	}
}

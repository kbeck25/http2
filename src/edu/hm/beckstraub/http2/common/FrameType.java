/*
HTTP 2 Server and Client
Hochschule M�nchen, Fakut�t 07 f�r Mathematik und Informatik
Netzwerke II, SS2015
Author: Kevin Beck, Alexander Straub
Date: 15.06.2015
*/
package edu.hm.beckstraub.http2.common;

import java.security.InvalidParameterException;

public enum FrameType
{
	Data('D'),
	Headers('H'),
	Priority('P'),
	Rst_Stream('R'),
	Settings('S'),
	Push_Promise('U'),
	Ping('I'),
	Goaway('G'),
	Window_Update('W'),
	Continuation('C');
	
	private final byte code;
	
	private FrameType(char code)
	{
		this.code = (byte)code;
	}
	
	public byte getCode()
	{
		return this.code;
	}
	
	public static FrameType decode(byte code) throws InvalidParameterException
	{
		for (FrameType ft: values())
		{
			if (ft.getCode() == code)
			{
				return ft;
			}
		}

		throw new InvalidParameterException("FrameType code doesn't exist");
    }
}
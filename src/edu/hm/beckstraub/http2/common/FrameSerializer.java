/*
HTTP 2 Server and Client
Hochschule M�nchen, Fakut�t 07 f�r Mathematik und Informatik
Netzwerke II, SS2015
Author: Kevin Beck, Alexander Straub
Date: 15.06.2015
*/
package edu.hm.beckstraub.http2.common;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.ArrayUtils;

public class FrameSerializer
{
	public static byte[] Serialize(Frame frame)
	{
		List<Byte> data = new ArrayList<>();
		
		// Add header
		byte[] header_length = Utilities.convertIntTo3Bytes(frame.getLength());
		byte header_type = frame.getType().getCode();
		byte header_flags = frame.getFlags();
		byte[] header_streamId = Utilities.convertIntTo4Bytes(frame.getStreamId());
		
		Utilities.addArray(data, header_length);
		data.add(header_type);
		data.add(header_flags);
		Utilities.addArray(data, header_streamId);
		
		// Add payload
		Utilities.addArray(data, frame.getPayload());
		
		return ArrayUtils.toPrimitive(data.toArray(new Byte[data.size()]));
	}
	
	public static Frame Deserialize(byte[] data) throws FrameException
	{
		// Read header
		int length = Utilities.convert3BytesToInt(new byte[] { data[0], data[1], data[2] });
		FrameType type = FrameType.decode(data[3]);
		byte flags = data[4];
		int streamId = Utilities.convert4BytesToInt(new byte[] { data[5], data[6], data[7], data[8] });
		
		// Read payload
		byte[] payload = Arrays.copyOfRange(data, 9, data.length);
		
		// Check if payload length is specified length in header
		if (length != payload.length)
		{
			throw new FrameException("Payload length isn't specified length in header");
		}
		
		Frame frame = new Frame(type, flags, streamId, payload);
		return frame;
	}
}
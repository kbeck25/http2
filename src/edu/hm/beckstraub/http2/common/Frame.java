/*
HTTP 2 Server and Client
Hochschule M�nchen, Fakut�t 07 f�r Mathematik und Informatik
Netzwerke II, SS2015
Author: Kevin Beck, Alexander Straub
Date: 15.06.2015
*/
package edu.hm.beckstraub.http2.common;

import java.security.InvalidParameterException;

public class Frame
{
	public static final int HEADER_LENGTH = 9;
	public static final int MAX_PAYLOAD_LENGTH = 16777216; // 2^24
	
	private final FrameType type;
	private final byte flags;
	private final int streamId;
	private final byte[] payload;
	
	public Frame(FrameType type, byte flags, int streamId, byte[] payload) throws InvalidParameterException
	{
		if (payload.length > MAX_PAYLOAD_LENGTH)
		{
			throw new InvalidParameterException("Payload length is to high");
		}
		
		this.type = type;
		this.flags = flags;
		this.streamId = streamId;
		this.payload = payload;
	}
	
	public int getLength()
	{
		return this.payload.length;
	}
	
	public FrameType getType()
	{
		return this.type;
	}
	
	public byte getFlags()
	{
		return this.flags;
	}
	
	public int getStreamId()
	{
		return this.streamId;
	}
	
	public byte[] getPayload()
	{
		return this.payload;
	}
}
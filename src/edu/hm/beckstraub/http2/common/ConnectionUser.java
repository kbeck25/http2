/*
HTTP 2 Server and Client
Hochschule M�nchen, Fakut�t 07 f�r Mathematik und Informatik
Netzwerke II, SS2015
Author: Kevin Beck, Alexander Straub
Date: 15.06.2015
*/
package edu.hm.beckstraub.http2.common;

public interface ConnectionUser 
{
	void headerComplete(Connection connection, Stream stream, Frame header);
	void dataComplete(Stream stream);
}

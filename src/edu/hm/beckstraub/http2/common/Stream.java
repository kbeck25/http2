/*
HTTP 2 Server and Client
Hochschule M�nchen, Fakut�t 07 f�r Mathematik und Informatik
Netzwerke II, SS2015
Author: Kevin Beck, Alexander Straub
Date: 15.06.2015
*/
package edu.hm.beckstraub.http2.common;

import java.security.InvalidParameterException;

public class Stream
{
	/* Fields */

	private final int id;
	private final Frame header;
	private byte[] data;
	private Frame priority;
	
	public boolean isClosed = false;
	
	/* Ctor */
	
	public Stream(Frame header)
	{
		if (header.getType() != FrameType.Headers && header.getType() != FrameType.Push_Promise)
		{
			throw new InvalidParameterException("Frame isn't of type Headers or Push_promise");
		}
		
		this.id = header.getStreamId();
		this.header = header;
		this.data = new byte[0];
	}
	
	/* Getter */
	
	public int getId()
	{
		return this.id;
	}
	
	public String getHeader()
	{
		return new String(this.header.getPayload());
	}
	
	public FrameType getHeaderType()
	{
		return this.header.getType();
	}
	
	public byte[] getData()
	{
		return this.data;
	}
	
	public int getPriority() throws FrameException
	{
		if (this.priority == null)
		{
			// Default priority if a priority frame wasn't received
			return 0;
		}
		
		if (this.priority.getPayload().length != 4)
		{
			throw new FrameException("Priority frame must have a length of 4");
		}
		
		return Utilities.convert4BytesToInt(this.priority.getPayload());
	}
		
	/* Public methods */
	
	public boolean addFrame(Frame frame)
	{
		if(this.isClosed) 
			return false;
		
		if (frame.getType() == FrameType.Data)
		{
			// Add payload to data			
			byte[] b = frame.getPayload();
			byte[] a = data;
			data = new byte[a.length + b.length];
			System.arraycopy(a, 0, data, 0, a.length);
			System.arraycopy(b, 0, data, a.length, b.length);
			
			if(frame.getFlags() % 2 == 1) // End of stream flag
			{
				this.isClosed = true;				
				return true;
			}
		}
		else if (frame.getType() == FrameType.Priority)
		{
			this.priority = frame;
		}
		else if (frame.getType() == FrameType.Headers)
		{
			if(frame.getFlags() %2 == 1)
			{
				// End Stream flag
				this.isClosed = true;
			}
		}
		// All other frame types aren't supported right now
		
		return false; // signal that not all data has been received
	}
}
/*
HTTP 2 Server and Client
Hochschule M�nchen, Fakut�t 07 f�r Mathematik und Informatik
Netzwerke II, SS2015
Author: Kevin Beck, Alexander Straub
Date: 15.06.2015
*/
package edu.hm.beckstraub.http2.common;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.ArrayUtils;

public class Connection implements Runnable
{
	private Socket client;
	private List<Stream> streams;
	private OutputStream os;
	private InputStream is;
	private ConnectionUser user;
	private int idCounter;
	
	public Connection(Socket socket, ConnectionUser user) throws IOException
	{
		this.client = socket;
		this.user = user;
		this.streams = new ArrayList<>();
		this.is = socket.getInputStream();
		this.os = socket.getOutputStream();
		this.idCounter = 0;
	}
	
	public void sendFrame(Frame frame)
	{	
		byte[] data = FrameSerializer.Serialize(frame);
		
		synchronized (this.os) 
		{
			try
			{
				this.os.write(data);
			}
			catch (IOException e)
			{
				System.out.println("Could not send response data: " + e.getMessage());
			}
		}
	}
	
	public void receiveFrame()
	{
		try (InputStream is = this.client.getInputStream(); 
		     OutputStream os = this.client.getOutputStream())
		{
			int readBytes;
			
			while (true)
			{
				byte[] data_header = new byte[Frame.HEADER_LENGTH];
				//waitForData(is, data_header.length);
				readBytes = is.read(data_header);
				if (readBytes < 0)
				{
					break;
				}
				
				// Read out payload length
				int payload_length = Utilities.convert3BytesToInt(
						new byte[] { data_header[0], data_header[1], data_header[2] });
				
				byte[] data_payload = new byte[payload_length];
				//waitForData(is, data_payload.length);
				readBytes = is.read(data_payload);
				if (readBytes < 0)
				{
					break;
				}
				
				byte[] data = ArrayUtils.addAll(data_header, data_payload);
				
				Frame frame = FrameSerializer.Deserialize(data);
				
				// Handle received frame
				if(frame.getType() == FrameType.Ping)
				{
					// Ping frames are not corresponding to a single frame
					handlePingFrame(frame, os);
				}
				else
				{
					this.addFrameToStream(frame);
				}
			}
		}
		catch (FrameException e)
		{
			System.out.println("FrameException in request: " + e.getMessage());
		}
		catch (InvalidParameterException e)
		{
			// Connection is over...
		}
		catch (IOException e)
		{
			System.out.println("IOException in request: " + e.getMessage());
		}
	}
	
	public static void waitForData(InputStream is, int dataLength) throws IOException
	{
		while (true)
		{
			if (is.available() >= dataLength)
			{
				Utilities.sleep(50);
				break;
			}
		}
	}
	
	public int getNextStreamId()
	{
		// get next
		this.idCounter++;
		
		// check if connection user already used it
		outerloop: while (true)
		{
			for (int i = 0; i < this.streams.size(); i++)
			{
				Stream stream = this.streams.get(i);
				if (stream.getId() == this.idCounter)
				{
					// Id is already in use, so try next id
					this.idCounter++;
					continue outerloop;
				}
			}
			
			return this.idCounter;
		}
	}
	
	private static void handlePingFrame(Frame frame, OutputStream os) throws FrameException, IOException
	{
		if(frame.getFlags() % 2 == 1)
		{
			// It is a ping response, so don't send a ping back
			System.out.println("Got ping response.");
		}
		else
		{
			// Otherwise response with a ping response frame
			byte[] opaqueData = new byte[8];
			Frame pingResponse = new Frame(FrameType.Ping, (byte)1, 0, opaqueData);
			byte[] data = FrameSerializer.Serialize(pingResponse);
			
			System.out.println("Got ping request with payload and send a response ping frame.");
			
			os.write(data);
			os.flush();
		}
	}
	
	private void addFrameToStream(Frame frame)
	{
		for (Stream stream: this.streams)
		{
			if (stream.getId() == frame.getStreamId())
			{
				// Stream found
				if (frame.getType() == FrameType.Rst_Stream)
				{
					// Remove stream when get a frame of type Rst_Stream
					//this.streams.remove(stream);
				}
				else
				{
					boolean dataComplete = stream.addFrame(frame);
					
					if(dataComplete)
					{
						// Notify user that data is complete
						user.dataComplete(stream);
					}
				}
				
				return;
			}
		}
		
		// Stream doesn't exist
		// Open a new stream if the frame is of type Headers or Push_Promise
		if (frame.getType() == FrameType.Headers || frame.getType() == FrameType.Push_Promise)
		{
			Stream stream = new Stream(frame);
			this.streams.add(stream);
			
			// Notify user that header is complete
			this.user.headerComplete(this, stream, frame);
		}
		else
		{
			throw new InvalidParameterException("There is no stream for this frame");			
		}
	}
	
	@Override
	public void run()
	{
		receiveFrame();
	}
}

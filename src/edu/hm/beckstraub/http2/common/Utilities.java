/*
HTTP 2 Server and Client
Hochschule M�nchen, Fakut�t 07 f�r Mathematik und Informatik
Netzwerke II, SS2015
Author: Kevin Beck, Alexander Straub
Date: 15.06.2015
*/
package edu.hm.beckstraub.http2.common;

import java.security.InvalidParameterException;
import java.util.List;

public class Utilities
{
	public static int convert3BytesToInt(byte[] bytes)
	{
		if (bytes.length != 3)
		{
			throw new InvalidParameterException("Bytes array must be size of 3");
		}
		
		return ((bytes[0] & 0xF) << 16) | ((bytes[1] & 0xFF) << 8) | (bytes[2] & 0xFF);
	}
	
	public static byte[] convertIntTo3Bytes(int integer)
	{
		byte[] bytes = new byte[3];
		for (int i = 0; i < 3; i++)
		{
		    bytes[2 - i] = (byte)(integer >>> (i * 8));
		}
		
		return bytes;
	}
	
	public static int convert4BytesToInt(byte[] bytes)
	{
		if (bytes.length != 4)
		{
			throw new InvalidParameterException("Bytes array must be size of 4");
		}
		
		return ((bytes[0] & 0xff) << 24) | ((bytes[1] & 0xff) << 16) |
			   ((bytes[2] & 0xff) << 8)  | (bytes[3] & 0xff);
	}
	
	public static byte[] convertIntTo4Bytes(int integer)
	{
		byte[] bytes = new byte[4];
		for (int i = 0; i < 4; i++)
		{
		    bytes[3 - i] = (byte)(integer >>> (i * 8));
		}
		
		return bytes;
	}
	
	public static void addArray(List<Byte> list, byte[] array)
	{
		for (byte b: array)
		{
			list.add(b);
		}
	}
	
	public static void sleep(long millis)
	{
		try
		{
			Thread.sleep(millis);
		}
		catch (InterruptedException e)
		{
			// this cannot happen...
		}
	}
}